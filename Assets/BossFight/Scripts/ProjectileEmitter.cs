using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileEmitter : MonoBehaviour
{
    public enum EmitterType
    {
        Static,
        Moving,
        Rotating
    }
    public Projectile projectilePrefab;
    public Transform emitterPoint;
    public float interval = 0.2f;
    public float projectileSpeed = 6f;
    public float moveOrRotationSpeed = 1f;
    public float maxAngle = 0f;
    public EmitterType emitterType;
    public bool isProjectile = true;
    private float movingRight = 1f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(shootProjectiles());
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeInHierarchy)
        {
            switch (emitterType)
            {
                case EmitterType.Static:
                    break;
                case EmitterType.Moving:
                    transform.position += Time.deltaTime * moveOrRotationSpeed * transform.right * movingRight;
                    break;
                case EmitterType.Rotating:
                    transform.localEulerAngles += new Vector3(0f, 0f, Time.deltaTime * moveOrRotationSpeed * 50 * movingRight);
                    if (maxAngle != 0f)
                    {
                        if (transform.localEulerAngles.z > maxAngle && transform.localEulerAngles.z < 360 - maxAngle)
                        {
                            if (movingRight == 1f)
                            {
                                transform.localEulerAngles = new Vector3(0f, 0f, maxAngle - 0.01f);
                            }
                            else
                            {
                                transform.localEulerAngles = new Vector3(0f, 0f, 360 - maxAngle + 0.01f);
                            }
                            movingRight = -movingRight;
                        }
                        //else if (transform.localEulerAngles.z > maxAngle && transform.localEulerAngles.z < 360 - maxAngle)
                        //{
                        //    movingRight = 1f;
                        //}
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private IEnumerator shootProjectiles()
    {
        while (true)
        {
            if (isProjectile)
            {
                Projectile proj = Instantiate(projectilePrefab, emitterPoint.position, Quaternion.AngleAxis(180, Vector3.forward) * emitterPoint.rotation);
                proj.Speed = projectileSpeed;
                yield return new WaitForSeconds(interval);
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        movingRight = -movingRight;
    }
}
