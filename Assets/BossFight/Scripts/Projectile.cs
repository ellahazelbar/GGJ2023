using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float Speed = 1f;
    public bool isMovingProjectile = true;
    public float LifeTime = 5f;
    public bool destructable = true;
    private float lifeTimer = 0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isMovingProjectile)
        {
            transform.position += Time.fixedDeltaTime * Speed * -transform.up;
        }
    }

    private void Update()
    {
        lifeTimer += Time.deltaTime;
        if (lifeTimer >= LifeTime)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (destructable)
        {
            Destroy(gameObject);
        }
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (destructable)
    //    {
    //        Destroy(gameObject);
    //    }
    //}
}
