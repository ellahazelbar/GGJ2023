using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BossFight
{
    public class HitTrigger : MonoBehaviour
    {
        public CharacterController Player;
        public BossBehaviourManager Boss;
        private void OnTriggerEnter(Collider other)
        {
            if (Player.Attacking && Boss.IsVulnerable)
                Boss.DamageBoss();
        }
    }
}