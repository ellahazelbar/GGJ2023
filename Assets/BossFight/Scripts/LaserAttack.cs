using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserAttack : MonoBehaviour
{
    public float Delay, RotationSpeed, RotationTime, PostDelay;

    private void OnEnable()
    {
        StopAllCoroutines();
        StartCoroutine(Go());
    }

    private IEnumerator Go()
    {
        yield return new WaitForSeconds(Delay);
        float startTime = Time.time, endTime = startTime + RotationTime;
        while (Time.time < endTime)
        {
            transform.rotation = Quaternion.AngleAxis(RotationSpeed * Time.deltaTime, Vector3.forward) * transform.rotation;
            yield return null;
        }
        yield return new WaitForSeconds(PostDelay);
        gameObject.SetActive(false);
    }
}
