using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using BossFight;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossBehaviourManager : MonoBehaviour
{
    [System.Serializable]
    public struct AttackBlock
    {
        public List<GameObject> Attacks;
        public float duration;

        public void EnableAttacks()
        {
            foreach (GameObject obj in Attacks)
            {
                obj.SetActive(true);
            }
        }
        public void DisableAttacks()
        {
            foreach (GameObject obj in Attacks)
            {
                obj.SetActive(false);
            }
        }
    }

    public Material ToothMaterial;
    public UnityEngine.UI.Image FadeToBlack;

    public bool IsVulnerable { get; set; }
    public float VibrationRadius = 0.1f;
    public List<AttackBlock> attacks;
    private int currentAttack;
    private int BossHealth = 3;
    private bool damaged = false;
    private float damageTimer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        IsVulnerable = false;
        StartCoroutine(FIGHT());
        ToothMaterial.SetInt("_EnableSpecialColor", 0);
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = VibrationRadius * Random.insideUnitCircle;
    }

    private IEnumerator FIGHT()
    {
        yield return new WaitForSeconds(3f);
        for (int i = 0; i < attacks.Count; i++)
        {
            AttackBlock attackBlock = attacks[i];
            attackBlock.EnableAttacks();
            yield return new WaitForSeconds(attackBlock.duration);
            attackBlock.DisableAttacks();
            if ((i + 1) % 3 == 0)
            {
                damageTimer = 0f;
                IsVulnerable = true;
                ToothMaterial.SetInt("_EnableSpecialColor", 1);
                while (!damaged && damageTimer < 7f)
                {
                    damageTimer += Time.deltaTime;
                    yield return null;
                }
                if (!damaged)
                {
                    i -= 3;
                }
                ToothMaterial.SetInt("_EnableSpecialColor", 0);
            }
            damaged = false;
            yield return new WaitForSeconds(1f);
        }
    }

    public void DamageBoss()
    {
        BossHealth--;
        damaged = true;
        if (BossHealth == 0)
        {
            StartCoroutine(FadeScenes(true));
        }
    }

    private IEnumerator FadeScenes(bool Passed)
    {
        float startTime = Time.time, endTime = startTime + 0.6f;
        yield return null;
        while (Time.time < endTime)
        {
            FadeToBlack.color = new Color(0, 0, 0, Mathf.InverseLerp(startTime, endTime, Time.time));
            yield return null;
        }
        FadeToBlack.color = Color.black;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + (Passed ? 1 : 0));
    }
}
