using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BossFight
{
    public class CharacterController : SingletonMonobehaviour<CharacterController>
    {
        public float UpAcceleration, SidewaysAcceleration, SidewaysDecceleration;
        public float RotationThreshold;
        public float AttackDuration = 0.75f;
        public bool Attacking { get; private set; }
        public TMPro.TMP_Text lifeText;
        public BossBehaviourManager BossManager;

        private Rigidbody rb;
        private bool Left, Right, Up;
        private float currentYRotation;
        private float Life = 4f;
        private bool invulnurable = false;
        void Start()
        {
            lifeText.text = Life.ToString();
            rb = GetComponent<Rigidbody>();
        }

        void Update()
        {
            GetInput();
            ProcessInput();
        }

        private void ProcessInput()
        {
            Vector3 acceleration = new Vector3();
            if (Up)
            {
                acceleration.y = UpAcceleration;
            }
            else acceleration.y = -5;
            if (Right != Left)
            {
                acceleration.x = Right ? SidewaysAcceleration : -SidewaysAcceleration;
            }
            else acceleration.x = -rb.velocity.x * SidewaysDecceleration;
            if (!Attacking)
            {
                currentYRotation = Mathf.MoveTowards(currentYRotation, acceleration.x > RotationThreshold ? -90 : acceleration.x < -RotationThreshold ? 90 : 0, 180 * Time.deltaTime);
                transform.rotation = Quaternion.AngleAxis(currentYRotation, Vector3.up);
            }
            rb.AddForce(acceleration, ForceMode.Acceleration);
        }

        private void GetInput()
        {
            if (!Up && Input.GetKeyDown(KeyCode.W))
            {
                Up = true;
            }
            else if (Up && Input.GetKeyUp(KeyCode.W))
            {
                Up = false;
            }
            if (!Right && Input.GetKeyDown(KeyCode.D))
            {
                Right = true;
            }
            else if (Right && Input.GetKeyUp(KeyCode.D))
            {
                Right = false;
            }
            if (!Left && Input.GetKeyDown(KeyCode.A))
            {
                Left = true;
            }
            else if (Left && Input.GetKeyUp(KeyCode.A))
            {
                Left = false;
            }
            if (!Attacking && Input.GetKeyDown(KeyCode.Space))
            {
                StartCoroutine(SwipeAttack());
            }
        }

        private IEnumerator SwipeAttack()
        {
            if (Left == Right)
                yield break;
            Quaternion startRotation = transform.rotation;
            float direction = Right ? -360 : 360;
            Attacking = true;
            float startTime = Time.time, endTime = startTime + AttackDuration;
            yield return null;
            while (Time.time < endTime)
            {
                transform.rotation = Quaternion.AngleAxis(Mathf.InverseLerp(startTime, endTime, Time.time) * direction, Vector3.up) * startRotation;
                yield return null;
            }
            transform.rotation = startRotation;
            Attacking = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Hazard") && !invulnurable)
            {
                StartCoroutine(invulnurability());
                Life--;
                lifeText.text = Life.ToString();
                if (Life == 0)
                {
                    StartCoroutine(FadeScenes(false));
                }
            }
        }

        private IEnumerator FadeScenes(bool Passed)
        {
            float startTime = Time.time, endTime = startTime + 0.6f;
            yield return null;
            while (Time.time < endTime)
            {
                BossManager.FadeToBlack.color = new Color(0, 0, 0, Mathf.InverseLerp(startTime, endTime, Time.time));
                yield return null;
            }
            BossManager.FadeToBlack.color = Color.black;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + (Passed ? 1 : 0));
        }

        private IEnumerator invulnurability()
        {
            invulnurable = true;
            yield return new WaitForSeconds(1f); 
            invulnurable = false;
        }
    }
}
