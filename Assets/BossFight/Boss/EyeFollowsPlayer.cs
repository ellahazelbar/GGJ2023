using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeFollowsPlayer : MonoBehaviour
{
    public Transform Target;
    
    void Update()
    {
        transform.rotation = Quaternion.LookRotation(Target.position - transform.position);       
    }
}
