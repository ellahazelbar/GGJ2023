using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GIfProjector : MonoBehaviour
{
    public Material DecalMaterial;
    public int Frame;
    public Texture2D[] Frames;
    public float FrameDelay;

    private float lastFrameTime;

    private void Start()
    {
        lastFrameTime = Time.time;
        NextFrame();
    }

    void Update()
    {
        DecalMaterial.SetTexture("Base_Map", Frames[Frame]);
       if (FrameDelay < Time.time - lastFrameTime)
        {
            NextFrame();
            lastFrameTime += FrameDelay;
        }
    }

    private void NextFrame()
    {
        Frame++;
        if (Frame == Frames.Length)
            Frame = 0;
    }
}
