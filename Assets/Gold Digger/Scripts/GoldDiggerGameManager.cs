using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoldDiggerGameManager : SingletonMonobehaviour<GoldDiggerGameManager>
{
    public TMPro.TMP_Text TeethCollectedText, LivesRemainingText;
    public UnityEngine.UI.Image FadeToBlack;
    public int TeethToCollect = 4;
    private int LivesRemaining = 3, TeethCollected = 0;
    public bool IsGameRunning { get; }
    // Start is called before the first frame update
    void Start()
    {
        TeethCollectedText.text = TeethToCollect.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (LivesRemaining == 0)
        {
            StartCoroutine(FadeScenes(false));
        }
        if (TeethCollected == TeethToCollect)
        {
            StartCoroutine(FadeScenes(true));
        }
    }

    public void ObjectCollected(CollectibleObject obj)
    {
        switch (obj.ObjectType)
        {
            case ObjectType.RottenTooth:
                TeethCollected++;
                TeethCollectedText.text = (TeethToCollect - TeethCollected).ToString();
                break;
            case ObjectType.NormalTooth:
                break;
            case ObjectType.Fly:
                TakeDamage();
                break;
        }
    }

    public void TakeDamage()
    {
        LivesRemaining--;
        LivesRemainingText.text = LivesRemaining.ToString();
    }

    private IEnumerator FadeScenes(bool Passed)
    {
        float startTime = Time.time, endTime = startTime + 0.6f;
        yield return null;
        while (Time.time < endTime)
        {
            FadeToBlack.color = new Color(0, 0, 0, Mathf.InverseLerp(startTime, endTime, Time.time));
            yield return null;
        }
        FadeToBlack.color = Color.black;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + (Passed ? 1 : 0));
    }
}
