using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerController : SingletonMonobehaviour<PlayerController>
{
    public float playerSpeed = 1f;
    public KeyCode ShootClawKey = KeyCode.Space;
    public float shootClawCooldown = 1f;
    public GoldDiggerGameManager GameManager;
    public float MaximumSwingAngle = 60f;
    public float SecondsToCompleteFullSwing = 2f;
    public AnimationCurve ClawMovementAnimationCurve;
    public GameObject RocketClawPrefab;
    public Transform playerAnchor;
    float swingTime = 0;
    private RocketClaw currentRocketClaw;
    // Start is called before the first frame update
    void Start()
    {
        currentRocketClaw = Instantiate(RocketClawPrefab, playerAnchor).GetComponent<RocketClaw>();
        currentRocketClaw.transform.localPosition = new(0, -1, -0.4f);
        currentRocketClaw.transform.localRotation = Quaternion.identity;
        currentRocketClaw.Player = this;
    }

    private void CreateRocketClaw()
    {
        currentRocketClaw = Instantiate(RocketClawPrefab, playerAnchor).GetComponent<RocketClaw>();
        currentRocketClaw.transform.localPosition = new(0, -1, -0.4f);
        currentRocketClaw.transform.localRotation = Quaternion.identity;
        currentRocketClaw.Player = this;
    }

    // Update is called once per frame
    void Update()
    {
        swingTime += Time.deltaTime / SecondsToCompleteFullSwing;
        float clawRotationAngle = ClawMovementAnimationCurve.Evaluate(swingTime) * MaximumSwingAngle;
        playerAnchor.eulerAngles = new(0, 0, clawRotationAngle);

        if (Input.GetKeyDown(ShootClawKey))
        {
            if (null != currentRocketClaw)
            {
                currentRocketClaw.Launch();
                currentRocketClaw = null;
                Invoke("CreateRocketClaw", shootClawCooldown);
            }
        }

        float moveDirection = Input.GetAxis("Horizontal");
        transform.position += new Vector3(moveDirection, 0f, 0f) * playerSpeed * Time.deltaTime;
        if (moveDirection != 0)
        {
            transform.eulerAngles = new Vector3(0, 90 * Mathf.Sign(moveDirection), 0);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("collision");
        //RocketClaw returningClaw = collision.gameObject.GetComponent<RocketClaw>();
        //if (null != returningClaw && returningClaw.gettingBack && null != returningClaw.AttachedObject)
        //{
        //    GameManager.ObjectCollected(returningClaw.AttachedObject);
        //    Destroy(collision.gameObject);
        //}
        if (collision.gameObject.CompareTag("Hazard"))
        {
            GameManager.TakeDamage();
        }
    }

    private void PERISH()
    {

    }
}
