using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterProjectile : MonoBehaviour
{
    public float flightSpeed = 3f;
    private Vector3 flightDirection;
    // Start is called before the first frame update
    void Start()
    {
        flightDirection = (PlayerController.Instance.transform.position - MonsterBehaviour.Instance.transform.position).normalized;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, flightDirection);
        StartCoroutine(DestroyAfterSeconds());
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += flightSpeed * Time.deltaTime * flightDirection;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Hazard"))
        {
            Destroy(collision.gameObject);
        }
        Destroy(gameObject);
    }

    private IEnumerator DestroyAfterSeconds()
    {
        yield return new WaitForSeconds(15);
        Destroy(this.gameObject);
    }
}
