using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterBehaviour : SingletonMonobehaviour<MonsterBehaviour>
{
    public GameObject SpitballPrefab;
    public int ProjectilesPerAttack = 3;
    public float ProjectileInterval = 1;

    private int attacksRemaining = 0;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LaunchProjectiles());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AngerMonster()
    {
        attacksRemaining += ProjectilesPerAttack;
        //PlayAngrySound
    }

    private IEnumerator LaunchProjectiles()
    {
        while (true)
        {
            if (attacksRemaining > 0)
            {
                Instantiate(SpitballPrefab, transform.position, Quaternion.identity);
                attacksRemaining--;
                yield return new WaitForSeconds(ProjectileInterval);
            }
            else
            {
                yield return null;
            }
        }
    }
}
