using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public enum ObjectType
{
    RottenTooth,
    NormalTooth,
    Fly
}


public class CollectibleObject : MonoBehaviour
{
    public ObjectType ObjectType;

    private Collider2D objectCollider;
    public void LatchOnToClaw(Transform claw, Vector3 latchPoint)
    {
        transform.parent = claw;
        transform.localPosition = latchPoint;
        objectCollider.enabled = false;
        if (ObjectType == ObjectType.NormalTooth)
        {
            MonsterBehaviour.Instance.AngerMonster();
        }
        else if (ObjectType == ObjectType.Fly)
        {
            GetComponent<FlyEnemy>().speed = 0;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        objectCollider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
