using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyEnemy : MonoBehaviour
{
    public float speed;
    public float MaxHorizontalDistanceFromStart = 1;
    public AnimationCurve MovementAnimationCurve;
    private Vector3 startingPosition;
    private float moveDirection = 1f;
    private float timeElapsed = 0;
    [Header("Fly")]
    public Transform FlyModel;
    public float VibrationRadius = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        startingPosition = transform.position;
    }

    void FixedUpdate()
    {
        timeElapsed += Time.fixedDeltaTime;
        float moveDirection = MovementAnimationCurve.Evaluate(timeElapsed);
        transform.position += moveDirection * Vector3.right * speed * Time.fixedDeltaTime;
        transform.localScale = new(Mathf.Sign(-moveDirection), 1f, 1f);
        FlyModel.localPosition = VibrationRadius * Random.insideUnitCircle;
    }
}
