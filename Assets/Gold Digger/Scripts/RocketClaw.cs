using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketClaw : MonoBehaviour
{
    public PlayerController Player;
    public float firingSpeed = 5.0f;
    public float retreivingSpeed = 3.0f;
    public Vector3 LatchPoint;
    private bool hasLaunched = false;
    private bool goingForward = false;
    public bool gettingBack { get; private set; }
    private Vector3 flightDirection;
    public CollectibleObject AttachedObject { get; private set; }
    private Collider2D thisCollider;

    // Start is called before the first frame update
    void Start()
    {
        thisCollider = GetComponent<Collider2D>();
        thisCollider.enabled = false;
        gettingBack = false;
    }

    void FixedUpdate()
    {
        if (hasLaunched)
        {
            if (goingForward)
            {
                flightDirection = -transform.up.normalized;
                transform.position += flightDirection * firingSpeed * Time.deltaTime;
            }
            else if (gettingBack)
            {
                flightDirection = (Player.transform.position - transform.position).normalized;
                transform.rotation = Quaternion.LookRotation(Vector3.forward, flightDirection);
                transform.position += retreivingSpeed * Time.deltaTime * flightDirection;
                if (Vector3.Distance(this.transform.position, Player.transform.position) < 1)
                {
                    GoldDiggerGameManager.Instance.ObjectCollected(AttachedObject);
                    Destroy(this.gameObject);
                }
            }
        }
    }

    public void Launch()
    {
        transform.parent = null;
        hasLaunched = true;
        goingForward = true;
        thisCollider.enabled = true;
        StartCoroutine(DestroyAfterSeconds());
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        CollectibleObject collectibleObject = other.collider.GetComponent<CollectibleObject>();
        if (null != collectibleObject && goingForward)
        {
            collectibleObject.LatchOnToClaw(transform, LatchPoint);
            AttachedObject = collectibleObject;
            goingForward = false;
            gettingBack = true;
        }
    }

    private void Explode(GameObject triggeringObject)
    {
        //ExplodeEffect
        Destroy(triggeringObject);
        if (null != AttachedObject)
        {
            Destroy(AttachedObject.gameObject);
        }
        Destroy(this);
    }

    private IEnumerator DestroyAfterSeconds()
    {
        yield return new WaitForSeconds(15);
        Destroy(this.gameObject);
    }
}
