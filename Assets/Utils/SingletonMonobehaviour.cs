using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonMonobehaviour<T> : MonoBehaviour where T : SingletonMonobehaviour<T>
{
    public static T Instance
    {
        get
        {
            if (null == _instance)
            {
                GameObject globals = GameObject.Find("Globals");
                if (null == globals)
                {
                    globals = new("Globals");
                }
                _instance = globals.AddComponent<T>();
            }
            return _instance;
        }
    }

    private static T _instance;

    protected void Awake()
    {
        _instance = (T)this;
    }
}
