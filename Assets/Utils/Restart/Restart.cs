using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : SingletonMonobehaviour<Restart>
{
    public void RestartLevel()
    {
        StartCoroutine(RestartLevelCRTN());
    }

    private IEnumerator RestartLevelCRTN()
    {
        Camera c = Camera.main;
        while (true)
        {
            c.fieldOfView -= 40 * Time.deltaTime;
            yield return null;
            if (c.fieldOfView < 5)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }
}
