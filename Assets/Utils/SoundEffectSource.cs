using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectSource : MonoBehaviour
{
    public void Initialize(AudioClip Clip)
    {
        AudioSource aud = GetComponent<AudioSource>();
        aud.clip = Clip;
        aud.Play();
        Destroy(gameObject, Clip.length);
    }
}
