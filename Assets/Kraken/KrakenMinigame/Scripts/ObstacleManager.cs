using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KrakenMinigame
{
    public class ObstacleManager : MonoBehaviour
    {
        public GameObject[] ObstaclePrefabs;
        public Material TargetMaterial;
        public AnimationCurve ScaleCurve;
        public AnimationCurve IntervalCurve;
        public CountdownTimer Timer;

        public float SpawnScale;
        public float StopSpawnTime = 2.5f;

        private void Start()
        {
            StartCoroutine(SpawnCoroutine());
        }

        private IEnumerator SpawnCoroutine()
        {
            yield return new WaitForSeconds(0.5f);
            float time = 0;
            SpawnObstacle();
            float nextSpawnTime = IntervalCurve.Evaluate(Time.timeSinceLevelLoad);
            while (true)
            {
                time += Time.deltaTime;
                if (nextSpawnTime < time)
                {
                    time -= nextSpawnTime;
                    SpawnObstacle();
                    nextSpawnTime = IntervalCurve.Evaluate(Time.timeSinceLevelLoad);
                }
                yield return null;
                if (Timer.TimeLeft < StopSpawnTime)
                {
                    yield break;
                }
            }
        }

        private void SpawnObstacle()
        {
            Instantiate(ObstaclePrefabs[Random.Range(0, ObstaclePrefabs.Length)]).GetComponent<Obstacle>().Initialize(SpawnScale, Random.Range(0f, 360f), ScaleCurve, TargetMaterial);
        }
    }
}
