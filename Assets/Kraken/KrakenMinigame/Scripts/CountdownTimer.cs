using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KrakenMinigame
{
    public class CountdownTimer : SingletonMonobehaviour<CountdownTimer>
    {
        public TMPro.TextMeshProUGUI TimerText;
        public UnityEngine.UI.Image TimerBG, FadeToBlack;
        public float TimeLeft;
        public float AppearTime = 30;
        private bool fading;

        void Update()
        {
            TimerText.enabled = TimerBG.enabled = TimeLeft < AppearTime;
            TimeLeft -= Time.deltaTime;
            TimerText.text = Mathf.Max(0, TimeLeft).ToString("00.00");
            if (!fading && TimeLeft < -2)
            {
                fading = true;
                StartCoroutine(FadeScenes());
            }
        }

        private IEnumerator FadeScenes()
        {
            float startTime = Time.time, endTime = startTime + 0.6f;
            yield return null;
            while (Time.time < endTime)
            {
                FadeToBlack.color = new Color(0, 0, 0, Mathf.InverseLerp(startTime, endTime, Time.time));
                yield return null;
            }
            FadeToBlack.color = Color.black;
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
}