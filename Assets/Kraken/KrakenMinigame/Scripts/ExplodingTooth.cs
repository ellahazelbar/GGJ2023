using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingTooth : MonoBehaviour
{
    public float Velocity = 10;
    void Start()
    {
        foreach (Rigidbody rb in GetComponentsInChildren<Rigidbody>())
        {
            Vector2 dir = Random.insideUnitCircle;
            rb.velocity = new Vector3(Velocity * dir.x, Velocity * dir.y, .5f);
        }
        Destroy(gameObject, 5);
    }
}
