using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingToothCreator : SingletonMonobehaviour<ExplodingToothCreator>
{
    public GameObject ExplodingToothPrefab;
    public AudioClip[] Crunches;
    public GameObject SoundEffectSourcePrefab;

    private int lastClipUsed = -1;

    public void Explode(GameObject Tooth)
    {
        GameObject newTooth = Instantiate(ExplodingToothPrefab);
        newTooth.transform.SetPositionAndRotation(Tooth.transform.position, Tooth.transform.rotation);
        newTooth.transform.localScale = (null != Tooth.transform.parent) ? Tooth.transform.parent.localScale : Vector3.one;
        newTooth.transform.SetParent(null, true);
        Destroy(Tooth);
        int clipIndex;
        do
        {
            clipIndex = Random.Range(0, Crunches.Length);
        } while (clipIndex == lastClipUsed);
        lastClipUsed = clipIndex;
        Instantiate(SoundEffectSourcePrefab).GetComponent<SoundEffectSource>().Initialize(Crunches[clipIndex]);
    }
}
