using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace KrakenMinigame
{
    public class Obstacle : MonoBehaviour
    {
        public Tooth[] Teeth;

        private AnimationCurve ScaleCurve;
        private bool disappearing;

        private float initialScale, scaleProgress;
        public void Initialize(float Scale, float Rotation, AnimationCurve ScaleCurve, Material TargetMaterial)
        {
            initialScale = Scale;
            transform.localScale = new Vector3(Scale, Scale, Scale);
            transform.rotation = Quaternion.AngleAxis(Rotation, Vector3.forward) * transform.rotation;
            if (0 < Teeth.Length)
                Teeth[Random.Range(0, Teeth.Length)].SetAsTarget(TargetMaterial);
            this.ScaleCurve = ScaleCurve;
        }

        private void Update()
        {
            scaleProgress += Time.deltaTime;
            float scale = ScaleCurve.Evaluate(scaleProgress);
            transform.localScale = new Vector3(scale * initialScale, scale * initialScale, scale * initialScale);
            if (!disappearing && scale < 0.002f)
                StartCoroutine(DisappearCoroutine());
        }

        private IEnumerator DisappearCoroutine()
        {
            disappearing = true;
            float alpha = 1;
            Renderer[] renderers = GetComponentsInChildren<Renderer>();
            foreach (Renderer ren in renderers)
            {
                ren.sharedMaterial = new Material(ren.sharedMaterial);
            }
            while (true)
            {
                foreach (Renderer ren in renderers)
                {
                    ren.sharedMaterial.SetFloat("Alpha", alpha);
                }
                alpha -= Time.deltaTime;
                yield return null;
                if (alpha < 0)
                {
                    Destroy(gameObject);
                    yield break;
                }    
            }
        }
    }
}