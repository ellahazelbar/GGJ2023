using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KrakenMinigame
{
    public class Tooth : MonoBehaviour
    {
        public Renderer[] Renderers;

        public bool IsTarget;

        public void SetAsTarget(Material TargetMaterial)
        {
            IsTarget = true;
            foreach (Renderer ren in Renderers)
            {
                //ren.sharedMaterial = TargetMaterial;
                ren.sharedMaterial = new Material(ren.sharedMaterial);
                ren.sharedMaterial.SetInt("_EnableSpecialColor", 1);
            }
        }

        public void Explode()
        {
            ExplodingToothCreator.Instance.Explode(gameObject);
        }

        [ContextMenu("Get Renderers")]
        public void GetRenderers()
        {
            Renderers = GetComponentsInChildren<Renderer>();
        }
    }
}