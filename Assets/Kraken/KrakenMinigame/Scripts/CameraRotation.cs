using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KrakenMinigame
{
    public class CameraRotation : MonoBehaviour
    {
        public float RotationSpeed;
        public float ChangeRotationProbability;
        public float OffsetRadius = 2;
        public float MovementSpeed = 0.5f;

        bool direction = true;

        private void Start()
        {
            StartCoroutine(OffsetCoroutine());
        }

        void Update()
        {
            if (Random.value < ChangeRotationProbability)
                direction = !direction;

            float rotation = direction ? -Time.deltaTime * RotationSpeed : Time.deltaTime * RotationSpeed;
            transform.rotation = Quaternion.AngleAxis(rotation, transform.forward) * transform.rotation;
        }

        private IEnumerator OffsetCoroutine()
        {
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(5, 8));
                float rad = Mathf.Pow(Random.value, 2) * OffsetRadius, ang = Random.Range(0, 2 * Mathf.PI);
                Vector3 targetPosition = new(Mathf.Cos(ang) * rad, Mathf.Sin(ang) * rad, transform.position.z);
                while (Mathf.Epsilon < Vector3.SqrMagnitude(transform.position - targetPosition))
                {
                    Vector3 position = Vector3.MoveTowards(transform.position, targetPosition, MovementSpeed * Time.deltaTime);
                    transform.SetPositionAndRotation(position, Quaternion.LookRotation(-position, transform.up));
                    yield return null;
                }
            }
        }
    }
}