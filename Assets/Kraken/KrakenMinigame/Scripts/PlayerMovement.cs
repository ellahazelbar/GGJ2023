using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KrakenMinigame
{
    public class PlayerMovement : MonoBehaviour
    {
        public float RotationSpeed = 360;
        private float radius, currentAngle = 90;

        private void Awake()
        {
            radius = transform.position.y;
        }

        private void Update()
        {
            bool inputReceived = false;
            if (Input.GetKey(KeyCode.A))
            {
                currentAngle += RotationSpeed * Time.deltaTime;
                inputReceived = true;
            }
            else if (Input.GetKey(KeyCode.D))
            {
                currentAngle -= RotationSpeed * Time.deltaTime;
                inputReceived = true;
            }

            if (inputReceived)
            {
                Vector3 position = new(radius * Mathf.Cos(currentAngle * Mathf.Deg2Rad), radius * Mathf.Sin(currentAngle * Mathf.Deg2Rad));
                transform.SetLocalPositionAndRotation(position, Quaternion.LookRotation(Vector3.forward, transform.position));
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            Tooth tooth = other.GetComponent<Tooth>();
            if (null != tooth)
            {
                foreach (Tooth t in other.transform.GetComponentInParent<Obstacle>().Teeth)
                {
                    t.GetComponent<Collider>().enabled = false;
                }
                if (tooth.IsTarget)
                {
                    tooth.Explode();
                }
                else
                {
                    ExplodingToothCreator.Instance.Explode(gameObject);
                    Restart.Instance.RestartLevel();
                }
            }
        }
    }
}