using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KrakenMinigame
{
    public class TempObstacle : MonoBehaviour
    {
        public AnimationCurve Scale;
        public bool Other;
        private float progress, max;

        private void Start()
        {
            max = Scale.keys[^1].time;
            Reinitialize();
            if (Other)
            {
                progress = 1.1f;
            }
        }

        private void Reinitialize()
        {
            transform.eulerAngles = new Vector3(0, 0, Random.Range(0f, 360f));
            progress = 0;
        }

        // Update is called once per frame
        void Update()
        {
            progress += Time.deltaTime;
            if (progress > max)
                Reinitialize();
            float scale = Scale.Evaluate(progress);
            transform.localScale = new Vector3(scale, scale, 1);
        }
    }
}