using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CutsceneManager : MonoBehaviour
{
    public int NextSceneIndex;
    public Texture2D[] Images;

    public RawImage[] Carrousel;
    public float FadeSpeed;

    private bool carrouselState;
    private int imageIndex;
    private bool busy;

    private IEnumerator Start()
    {
        busy = true;
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(FadeImagesCoroutine());
        foreach (Soundtrack st in FindObjectsOfType<Soundtrack>())
        {
            Destroy(st.gameObject);
        }
    }

    private void Update()
    {
        if (!busy && Input.GetMouseButtonDown(0))
        {
            if (imageIndex < Images.Length)
                StartCoroutine(FadeImagesCoroutine());
            else
            {
                StartCoroutine(FadeToBlack());
            }
        }   
    }

    private IEnumerator FadeImagesCoroutine()
    {
        busy = true;
        RawImage current = Carrousel[carrouselState ? 0 : 1], prev = Carrousel[carrouselState ? 1 : 0];
        current.texture = Images[imageIndex];
        float progress = 0f;
        yield return null;
        while (progress < 1f)
        {
            if (Input.GetMouseButtonDown(0))
            {
                break;
            }
            progress += FadeSpeed * Time.deltaTime;
            current.color = new Color(1, 1, 1, progress);
            float prevColor = prev.color.r;
            prev.color = new Color(prevColor, prevColor, prevColor, 1 - progress);
            yield return null; 
        }
        current.color = Color.white;
        prev.color = new Color(1, 1, 1, 0);
        imageIndex++;
        carrouselState = !carrouselState;
        busy = false;
    }

    private IEnumerator FadeToBlack()
    {
        float progress = 0f;
        yield return null;
        while (progress < 1f)
        {
            progress += FadeSpeed * Time.deltaTime;
            Carrousel[0].color = Carrousel[1].color = new Color(1 - progress, 1 - progress, 1 - progress, 1);
            yield return null;
        }
        SceneManager.LoadScene(NextSceneIndex);
    }
}
